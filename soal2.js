const checkTypeNumber = (givenNumber) => {
    let resultNumberCheck

    if (typeof givenNumber == "number") { 
        if (givenNumber % 2 == 0) {
            resultNumberCheck = "GENAP"
        } else {
            resultNumberCheck = "GANJIL"
        }
    } else if (givenNumber == null) { // Validasi jika parameter Null
        resultNumberCheck = "Error : Bro where is the parameter?"
    } else { // Tipe data salah
        resultNumberCheck = "Error : Invalid data Type"
    }

    return resultNumberCheck
}

console.log(checkTypeNumber(12))

